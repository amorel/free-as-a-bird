---
permalink: /about/
title: "À propos de ce blog"
toc: true
---
Ce site est un site perso et n'exprime que le point de vue de son auteur. On y applique les principes du [Développement Zéro Déchet](http://cbossard.page.nuiton.org/presentation-dev-zero-dechet/#1), vous pouvez [le forker](/forkme/) pour y contribuer des posts et/ou créer votre propre blog.

Vous pourrez suivre ici les tribulations d'un développeur qui découvre Linux, les logiciels libres et les entreprises libres.

## Entreprise libre ? Kézako ?

Code Lutin (ma boîte) fait partie du réseau [Libre Entreprise](https://www.libre-entreprise.org/charte), dont je vous invite à consulter la charte qui présente très bien le concept d'entreprise libre.

Pour résumer et d'après ma compréhension, une Entreprise libre est un groupe auto-géré d'individus qui lient leur sort au sein d'une structure à taille humaine pour que chacun puisse choisir sa façon de vivre et de travailler. On y est particulièrement vigilant à la tenue démocratique de l'entreprise (on vote, 1 personne = 1 voix), au partage équilibré du chiffre d'affaire (à Code Lutin, rémunération strictement équivalente entre les lutins) et à la transparence (toutes les décisions et données sont à disposition de tous les membres de l'entreprise).

C'est un principe qui m'attire depuis longtemps, ayant toujours trouvé les liens hiérarchiques peu propices à l'épanouissement personnel et collectif (que l'on soit en haut ou en bas de la pyramide). Je pense aborder dans ce blog mon expérience quotidienne en tant que membre d'une entreprise libre, qu'il s'agisse de ses avantages ou de ses écueuils.

## Logiciels Libres ? Pourquoi ?

Les logiciels libres peuvent, selon moi, être un des piliers permettant la construction de solutions pérennes et d'un développement logiciel durable, loin de la course énergivore à l'innovation ou liberticide au tracking. Ma pensée est encore en construction sur ce sujet, il s'agira probablement d'un des thèmes abordés régulièrement dans ce blog. [Ce post de Framasoft](https://framablog.org/2020/04/02/prendre-de-la-hauteur/) me semble être un bon point de départ pour réfléchir au rôle du libre.

## A propos de moi

J'ai fait mes études à l'université de Nantes, pour laquelle je donne maintenant des cours en Master.

Avant de rejoindre Code Lutin, j'ai passé 6 ans chez [Obeo](https://www.obeo.fr/fr/) à développer des plugins Eclipse pour outiller la démarche MDE (génération de code, partage de modèles, outils de modélisation...). J'ai notamment travaillé sur les [fonctionnalités collaboratives en temps-réel du projet Sirus](https://www.obeodesigner.com/en/collaborative-features) et j'ai leadé le projet [Mylyn Intent](https://projects.eclipse.org/projects/mylyn.docs.intent), une tentative d'appliquer les concepts du Literate Programming au MDA. Le projet est maintenant au point mort, je n'ai pas réussi à fédérer une communauté suffisante autour bien que je sois encore convaincu de la pertinence de cet outil.

Je suis ensuite devenu CTO d'[iRéalité](http://irealite.com), la cellule informatique de Capacités SAS. Entre diverses autres choses (on touchait à pas mal de trucs), j'y ai fait du développement mobile Android (Kotlin, Android Architecture Components), iOS (Swift, Real), Web (Angular), Serveur (Node.js), Réalité Augmentée (Unity/Vuforia), Deep Learning (TensorFlow 2), Dual-Screen tablette-PC en bluetooth, interfaçage Leap Motion en JS, géolocalisation indoor avec des beacons... J'ai également appris à animer et fédérer une équipe, évaluer les risques d'un projets, rédiger des appels d'offres... et aussi à prendre soin de moi parce que le rythme de travail pouvoir être intense :)

J'ai rejoins Code Lutin en Mai 2020, au beau milieu du confinement, séduit par leur approche d'entreprise libre et les multiples contributions humaines et financières que l'entreprise apporte à différentes associations.

## Autres projets

En dehors de l'informatique, je me passionne entre moults sujets pour la [guitare](https://soundcloud.com/user-651566508), la parentalité positive ([Céline Alvarez](https://www.celinealvarez.org/), [Catherine Gueguen](https://www.laurencepernoud.com/enfant-1-3-ans/psychologie-petit-enfant/catherine-gueguen-si-on-changeait-regard-sur-lenfant.html), [Isabelle Filliozat](https://www.youtube.com/watch?v=x1pHlTRVzRM)), la [Révolution française](https://www.youtube.com/watch?v=LuOsYczMO1M&list=PLsFVQLtN7crZKK4rO-wpPsAT4wdLLL_2B), le boudhisme, la politique. J'aime bien nager aussi mais je suppose que vous vous en fichez.