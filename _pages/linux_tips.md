---
permalink: /cheatsheets/
title: "Cheatsheets & tips"
toc: true
---

# ZSH / OhMyZSH

* [OhMyZSH plugins fréquents](https://github.com/ohmyzsh/ohmyzsh/wiki/Cheatsheet)


# Git / Gitflow

* [Gitflow : concept et commandes](https://danielkummer.github.io/git-flow-cheatsheet/)
* [Gitflow : commandes avancées](https://github.com/petervanderdoes/gitflow-avh#git-flow-usage)

# Linux

## Commandes pour installer des trucs

* `apt policy <package name>` versions dispo / installées du package
* `dpkg -l | grep mumble <package name>` chercher parmis les package installés
* `sudo dpkg -i <ficher deb>` installer un package télécharger à la main
* `sudo add-apt-repository <repo-url>` ajouter un repo aux packages utilisés dans le apt-get install

## Commandes utiles quand on fait n'importe quoi avec sudo

* `ls -al <folder>` vérifie qui est proprio du folder
* `sudo chown -R <username>:<folder>` réatribue le folder au bon user

## Mail catcher pour serveurs locaux

* `docker run -p 41080:80 -p 41025:25 -d --name maildev --rm djfarrelly/maildev`
* Adresse du SMTP : localhost:41025 
* Accès à l'IHM via : localhost:41080


# Raccourcis

## Interface graphique (Ubuntu)

`Alt + F8` Pour rentrer en mode resize d'une fenêtre

