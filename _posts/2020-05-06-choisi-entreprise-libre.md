---
title: "J'ai choisi l'entreprise libre"
date: 2020-05-06T22:12:30-04:00
toc: true
categories:
  - entreprise_libre
  - logiciel_libre
header:
  teaser: /assets/images/post1/teaser.gif
---
J'ai eu le plaisir de rejoindre en début de semaine les équipes de [Code Lutin](https://codelutin.com) (je travaillais depuis 6 ans en tant que CTO d'iRéalité - [tout est détaillé là](/free-as-a-bird/about/#a-propos-de-moi)). Il me semble intéressant, en guise de premier article et pour qu'on apprenne à se connaitre, de revenir sur les raisons qui m'ont poussées à rejoindre cette entreprise, et par là même le [réseau Libre-Entreprise](http://www.libre-entreprise.org/) et l'[April](https://www.april.org/), entre autres associations au sein desquelles agit Code Lutin.

Mon objectif avec ce billet est de partager avec vous, lecteur, mes interrogations sur certains aspects de nos conditions de travail et à la finalité de ce que nous produisons, ainsi que les raisons qui me permettent de croire que les entreprises et logiciels libres pourraient apporter une partie de la réponse.

> **Disclaimer** : il est certain que je changerais d'avis sur bien des points que j'aborde ici, voire que mes propos sembleront stupides pour des devs évoluant dans le libre depuis longtemps. Mais justement, je pense que c'est le moment rêvé pour écrire cet article, tant que je suis nimbé de l'ignorance des profanes :)

Bonne lecture, n'hésitez pas à [suivre le tutoriel pour forker ce blog](/forkme/) si le design vous plaît, à me faire part de vos commentaires [par mail](mailto:lagardealex@gmail.com), sur [Twitter](https://twitter.com/alex_morel_), [Mastodon](https://mastodon.libre-entreprise.com/@amorel) ou à [lever un ticket](https://gitlab.nuiton.org/amorel/free-as-a-bird/issues/new) si vous relevez des erreurs d'orthographe, de sens ou de css :)

## Entreprise et hiérarchie
{% include figure image_path="/assets/images/post1/1.gif" alt="Chef d'équipe appliquant la méthode SCRUM avec vigueur" caption="Chef d'équipe appliquant la méthode SCRUM avec vigueur" %}
### Marre des pyramides !
Mon désir de rejoindre Code Lutin part avant tout de ma propre expérience du monde du travail. J'y ai été tour à tour subordonné ou chef d'équipe, et même si je prends un énorme plaisir à transmettre mes connaissances ou à aspirer celles des collègues en les harcelant de questions, je trouve certains aspects du fonctionnement hiérarchique d'une entreprise "classique" un peu contradictoires :
* On bosse en équipe tout au long de l'année, pourtant à chaque entretien il faut défendre son bout de gras pour négocier son salaire **individuellement**. J'ai toujours été très géné à l'idée de gagner plus que mes collègues en tant que CTO, alors que selon moi ils travaillent au moins aussi dur et sont au moins aussi compétents. J'ai aussi du mal avec les commerciaux et directeurs qui se rémunèrent royalement sans que cela soit justifié en matière d'activité (ok ils rapportent en partie les contrats, mais il faut les produire derrière). Mention spéciale aux graphistes qui se prennent 100% du stress des deadlines et des retours clients pour une rémunération souvent aux fraises (mais qu'ils s'estiment heureux d'avoir un CDI quand même)
* Corollaire de la différence de salaire et des liens hiérarchiques: cela entraine tacitement des dynamiques unidirectionnelles de type "sachants" -> "apprenants", alors qu'il est rare qu'un junior n'ait pas aussi des choses à apprendre aux seniors. Cela freine la diffusion du savoir et des pratiques au sein des équipes
* Même s'il arrive dans les bonnes boîtes que l'équipe dirigeante soit "à l'écoute" des suggestions, de facto c'est en haut qu'on décide (qu'il s'agisse de décisions commerciales, stratégiques, techniques)
* les salariés qui n'ont pas accès à toutes les informations (CA, comptabilité, prévisionnel) sont **déresponsabilisés** du devenir de l'entreprise. De plus sous prétexte qu'ils n'ont "pas tous les éléments" ils peuvent être **décrédibilisés** dans leur critique des choix stratégiques
* Le vouloir des salariés et le vouloir de l'entreprise (le **"Désir Maitre"**) ne sont pas forcément identiques, loin de là. [Frédéric Lordon](https://fr.wikipedia.org/wiki/Fr%C3%A9d%C3%A9ric_Lordon), philosophe et économiste français, parle d'**Angle Alpha** pour désigner l'écart entre les souhaits/intérêts d'un employé et ceux de l'entreprise.

Frédéric Lordon montre comment nos sociétés modernes façonnent et limitent notre conception du travail et de l'entreprise. Plutôt que de me lancer dans un long discours, je vous invite à consulter cette [fantastique vidéo d'Usul](https://peertube.heraut.eu/videos/watch/8af79fce-4c47-4809-ae7f-74386b52eb1a) qui constitue une parfaite initiation à la pensée de Frédéric Lordon.

### Entreprise libre
C'est entre autres pour ces raisons que j'ai été séduit par le modèle d'[entreprise libre](http://www.libre-entreprise.org/), développé et porté par des entreprises comme Code Lutin. Ce type d'entreprise s'appuie sur des principes et des engagements permettant sur le papier d'éviter les écueils mentionnés plus haut (reste pour moi à vérifier leur application réelle, même si les témoignages que j'ai pu recueillir semblent confirmer cette hypothèse) :

**Démocratie**
* Prise des décisions par l'ensemble des salariés réunis en **assemblée générale**, sur le principe 1 personne = 1 voix (ce qui n'empêche pas de désigner des responsables, mais rend leur statut juste et révocable)
* Répartition équilibrée du capital : Code Lutin pousse ce principe à fond en proposant une **égalité stricte de rémunération**, et ce peu importe l'expérience, le sexe, la compétence ou sa capacité à négocier le bout de gras. L'utilisation des éventuels bénéfices (dons à des associations, ré-investissement, partage équitable entre salariés) est décidée collectivement

**Transparence**  

Chaque salarié a accès à toutes les informations de l'entreprise : comptabilité, montants des contrats etc... ce qui **reponsabilise** chaque membre (la bonne tenue de la boîte est le problème de tout le monde) et aide à prendre des décisions éclairées.

Nous voyons donc ici en quoi les entreprises libres pourraient solutionner une partie des problèmes liés à la hiérarchie des entreprises et au rapport des salariés à leur travail. Un autre point qui me pose beaucoup question depuis quelques années est celui du sens de notre activité.

## Pourquoi on dev ?
{% include figure image_path="/assets/images/post1/2.gif" alt="Pendant que le monde brûlait, il fixait des NPE" caption="Pendant que le monde brûlait, il fixait des NPE" %}

### Besoin de sens
Passé un début de carrière où ma priorité était de prouver au monde et à moi-même que j'étais compétent, petit à petit elle glisse vers une recherche de sens à mon activité. La plupart de mes amis font des métiers utiles comme bosser dans l'alimentaire, assistants sociaux, profs, médecins et compte tenu de ce qu'ils apportent à la société quel putain de scandale qu'on soit plus payés en informatique que la plupart des gens, à coder des trucs.

C'est très difficile à accepter, mais nous sommes à un point crucial de l'histoire humaine où il va falloir faire les bons choix et vite. Je pense que dans quelques dizaines d'années on regardera avec beaucoup de sévérité notre génération si nous ne nous posons pas la question de la finalité de nos actions dans un contexte d'effondrement écologique et systémique probable. Ce n'est pas le sujet mais je ne peux que vous conseiller cette [vidéo d'Aurélien Barrau](https://peertube.fr/videos/watch/7f1185f5-6cf6-47e1-98e3-e1b736688390) au sujet du défi que représente notre époque.

Dans ce contexte, l'utilisation qui est faite de l'informatique dans notre société pose problème à de nombreux niveaux :
* **Consommation énergétique** en roue libre (4% des gaz à effet de serre émis par notre consommation numérique, accroissement de 9% par an d'après [cette étude](https://www.blogdumoderateur.com/numerique-emet-gaz-effet-serre/)) 
* Captation des données personnelles et **tracking** GPS (il n'y a qu'à voire les velléités totalitaires qui s'expriment lorsqu'on parle de track/quer les malades du coronavirus pour en saisir le danger)
* Mise en place d'**algorithmes inhumains**. J'ai à ce sujet une anecdote personnelle : je présentais le Deep Learning à un politique bien placé et en capacité de décision à grande échelle. Ses deux premières demandes d'applications prioritaires étaient la détection d'âge à partir de photos afin de déterminer si un sans-papiers était mineur (pour pouvoir le reconduire à la frontière, la loi interdisant la reconduite d'enfants mineurs et aucun médecin n'acceptant d'évaluer l'âge des sans-papiers par éthique) et la création d'un algorithme pour identifier les bâtiments sociaux qu'il était possible de fermer (pardon *"optimiser les flux compte tenu de la numérisation des démarches administratives"*). C'est dire la vision qu'ont certains politiques du but de l'outil informatique et de l'intérêt général...
* Pouvoir des **GAFAMs** menaçant voire se substituant aux structures politiques (cf [l'excellent billet de pyg de l'équipe FramaSoft](https://framablog.org/2020/04/02/prendre-de-la-hauteur/)  sur la toxicité des GAFAMs et l'après-confinement)
* **Dispersion** idiote de l'effort de développement : à échelle planétaire, combien de développeurs ont mis au point exactement le même framework utilitaire propriétaire ?
* **Finalité** des outils développés : combien servent à rendre le monde meilleur ou à favoriser l'entraide (et par qui sont-ils financés), combien servent à fliquer, faire du profit à tout prix, exploiter les gens et les ressources (et par qui sont-ils financés) ?

Il m'a plusieurs fois traversé l'esprit d'arrêter l'informatique, déçu comme j'ai pu l'être par le milieu universitaire, qui pourrait clairement apporter un contre-pouvoir intéressant aux entreprises traditionnelles en mettant en avant la science et l'humain, mais qui se retrouve perdu dans des luttes de pouvoir et jeux politiques le rendant inapte à répondre à la mesure des enjeux.

Cependant, je reste convaincu que l'informatique pourrait constituer un des piliers du **"Monde d'après"**. Reste que tant que la stratégie de développement de solutions informatiques reste à l'échelle des entreprises, seule la rentabilité sera recherchée. Même s'il existe des contre-exemples heureux, comme la superbe initiative du collectif [Makair](https://t.co/zmnBXtkMu1?amp=1) qui a permis de fabriquer des respirateurs lowcost et opensources, on ne peut pas dire qu'ils soient la norme ni que le système actuel soit idéal pour encourager de telles initiatives. Tant qu'aucune structure/collectif de pilotage et de contrôle avec des pouvoirs réels (e.g. la CNIL n'a qu'un rôle consultatif) n'existera, la course à l'innovation matérielle et logicielle (quitte à créer de l'obsolescence volontairement) se poursuivra sans état d'âme. Comment permettre aux développeurs, et plus généralement à l'ensemble des citoyens, d'oeuvrer ensemble pour construire des solutions **durables** (et donc non-propriétaires) ?

Je vous invite à regarder cette [super présentation des Codeurs en Libertés](https://video.passageenseine.fr/videos/watch/a6a71871-7dd3-4f9e-bd6e-3893cdb46c88) qui approfondit les quelques éléments de réflexion évoqués ici.

### Le libre, un contre-pouvoir ?

J'ai déjà, de par mes années de pur bonheur (no joke) passées chez [Obeo](https://www.obeo.fr/fr/) (un membre stratégique de la fondation Eclipse), une expérience de l'open-source. J'ai eu le chance de participer en tant que speaker à plusieurs conférences communautaires (ce qui se passe à Eclipsecon reste à Eclipsecon :D), de leader un projet open-source et donc de découvrir la nécessité de fédérer une base de contributeurs et d'utilisateurs. Mais de part ma jeunesse d'esprit je pense être passé à l'époque à côté de la portée militante du libre, d'à quel point il peut faire partie d'un contre-pouvoir aux GAFAM.

C'est ainsi que j'ai découvert qu'il existait des associations ambitieuses comme l'[April](https://www.april.org/), qui propose une action double : 
* **Démocratiser** et diffuser les logiciels libres via un travail de vulgarisation auprès du grand public (pour qu'on arrête d'utiliser des outils propriétaires dangereux et pétés)
* Effectuer un travail de **lobbying** (bon eux préférent le terme positif d'[advocacy](https://www.april.org/difference-entre-lobbying-et-advocacy) mais c'est pour que vous compreniez l'idée) auprès des institutions politiques à différentes échelles, pour convaincre de l'intérêt d'adopter et financer des solutions libres et pérènnes plutôt que de dilapiter l'argent du contribuable en licences annuelles

Récent adhérent de l'association, il me reste encore du chemin à faire pour en saisir tous les enjeux, mais je vous invite à écouter les podcasts de l'émission hebdomadaire [Libre à vous](https://april.org/les_podcasts_libre_a_vous) l'ambiance y est relax et les sujets passionants. La [récente interview de Maxime Guedj pour france info](https://www.francetvinfo.fr/culture/livres/internet-peut-redevenir-un-bien-commun-si-nous-nous-emparons-des-outils-alternatifs-aux-gafam-entretien-avec-maxime-guedj-co-auteur-de-declic_3919083.html) résume bien les enjeux libre vs GAFAM.

À échelle plus locale, l'association [Alliance Libre](http://www.alliance-libre.org/) regroupe différents acteurs du libre en Pays de la Loire pour mutualiser les efforts et échanger sur les pratiques.

Je n'ai pas encore pleinement intégré la philosophie libriste dans ma vision du monde. J'ai hâte d'échanger au sein de l'April, d'Alliance libre et avec les lutins pour affiner ma pensée sur le rôle que devrait jouer le libre dans le monde de demain, et par quels moyens favoriser son adoption face aux solutions propriétaires et leur armée de lobyies.

## On fait quoi des sous ?

{% include figure image_path="/assets/images/post1/3.gif" alt="SHUT UP AND TAKE MY MONEY !" caption="SHUT UP AND TAKE MY MONEY !" %}
Un autre problème qui me titille depuis longtemps est que les entreprises captent et gèrent une immense partie du PIB. Le premier aspect qui m'a attiré chez Code Lutin est la façon qu'ils ont de **redistribuer** les bénéfices (hors investissement dans l'outil de production). 

Dans toute entreprise informatique classique, 3 options sont généralement adoptées :
* pas de redistribution au salarié (à l'ancienne)
* redistribution partielle sous forme de prime en fin d'année
* et/ou redistribution partielle sous forme d'intéressement (la grande mode pour faire disruptif)

Code Lutin a une autre façon de fonctionner (bien sûr la répartition est votée en AG) :
* **Mécénat** auprès d'association oeuvrant dans le libre sur des sujets sortant du domaine d'expertise de Code Lutin ([liste des dons disponible ici](https://mastodon.libre-entreprise.com/tags/M%C3%A9c%C3%A9natCodeLutin)) : déGAFAMisation des OS mobiles, réseau social libre mastodon, fournisseurs d'accès à internet assocatifs, associations de vulgarisation autour du libre, collectifs de défense contre la censure...
* **Redistribution** égalitaire auprès des salariés

Je pense qu'il est très important d'acter le fait qu'actuellement la richesse produite par les entreprises n'est pas assez redistribuée et qu'elle finit bien trop dans les bulles spéculatives et pas assez dans l'économie réelle (ne parlons même pas de projets sociaux). D'un autre côté, beaucoup d'associations ont le savoir-faire mais ont besoin de fonds supplémentaires pour pouvoir être ambitieuses dans leur action et ne pas se baser que sur le bénévolat. C'est pourquoi il me semble très intéressant de déployer à échelle d'entreprise une politique de mécénat. C'est d'ailleurs un tweet de Code Lutin à propos d'un don fait à des associations qui m'a poussé à les contacter en premier lieu.

## L'aspect humain
Bon je pense avoir fait le tour des raisons intellectuelles qui m'ont poussé à choisir de quitter mon job actuel et tenter l'aventure d'une entreprise libre qui fait du libre.

Mais dans ce genre de choix ce n'est jamais l'intellect qui prime. En plus de ma sympathie pour les valeurs portées par Code Lutin, c'est bien sûr le facteur humain qui a joué. Et là difficile d'être rationnel, c'est tout un tas de petits éléments mis bouts à bout, les récentes conférences des lutins (notamment une portant sur le [Développement Zéro Déchet](http://cbossard.page.nuiton.org/presentation-dev-zero-dechet/#1)), leur politique de mécénat, les profils croisés sur twitter et IRL (fan de comics, de rugby, de vélo, bassiste...), les discussions lors des entretiens techniques... *It's a feeling* comme dirait Jean Claude Vandamme. Par rapport aux autres structures chez lesquelles j'avais postulé, j'ai tout de suite senti une diversité de profil (âge, sexe, expérience), de discours no-bullshit et un "esprit" qui différait grandement de ce que j'avais vu ailleurs.

## Conclusion
J'ai conscience de n'avoir enfoncé ici que des portes ouvertes, mais je ressentais le besoin de poser par écrit toutes ces réflexions qui fourmillent dans ma tête. Je pense affiner au fur et à mesure les différents points évoqués ici au contact des acteurs du libres. Ce billet pourra être une milestone intéressante vers laquelle revenir pour comparer l'évolution de mon avis.

J'adore ces moments où on a le sentiment de s'aventurer vers l'inconnu. C'est un tout nouveau monde qui s'offre à moi, et je suis ravi de faire le voyage en compagnie des lutins :) J'ai hâte de voir ce que la suite me réserve, d'affiner toutes les idées abordées ici un peu en vrac, de rencontrer les acteurs du libres, d'apprendre des nouvelles stacks techniques, de participer aux AG Lutines...

Merci d'avoir pris le temps de lire cet article. Le prochain post traitera probablement du choix de la distribution de Linux quand on n'y connait rien, parce que c'est assez fun comme sujet. N'hésitez pas à me faire part de vos commentaires [par mail](mailto:lagardealex@gmail.com), sur [Twitter](https://twitter.com/alex_morel_), [Mastodon](https://mastodon.libre-entreprise.com/@amorel) ou à [lever un ticket](https://gitlab.nuiton.org/amorel/free-as-a-bird/issues/new) si vous relevez des erreurs d'orthographe ou sens.

À bientôt !

P-S : si vous êtes UX Designer et que mon billet vous parle, [Code lutin est intéressé venez boire un café](https://www.codelutin.com/page-offre-demploi-UX-design.html) :)

## Compléments de lecture
Voici quelques liens qui me semblent intéressant pour poursuivre les thèmes évoqués dans ce billet : 

* **Fréderic Lordon** - la [vidéo d'Usul qui vulgarise la pensée de Lordon](https://peertube.heraut.eu/videos/watch/8af79fce-4c47-4809-ae7f-74386b52eb1a) est incontournable. On y parle de désir maitre des entreprises, de libre arbitre, de Spinoza, c'est juste passionnant et très digeste, comme toute la série "Nos chers contemporains" que je vous conseille les yeux fermés
* **Codeur-se-s en Liberté** - une conférence de Codeur-se-s en liberté qui présente comment ils ont pensé leur structure pour échapper à la machine à broyer du salarié qu'est l'entreprise moderne (même scope que ce billet mais beaucoup plus clair et poussé) : [Codeur-se-s de tous pays, unissez-vous !](https://video.passageenseine.fr/videos/watch/a6a71871-7dd3-4f9e-bd6e-3893cdb46c88)
* **Prendre de la hauteur** - analyse de la crise du Covid par le prisme du libre, cet
[excellent billet de l'équipe de Framablog](https://framablog.org/2020/04/02/prendre-de-la-hauteur/) m'a pas mal secoué, je n'ai pas fini de le digérer et compte bien lire d'autres posts de pyg
* **Interview de Maxime Guedj** - article de franceinfo dans lequel les enjeux libre vs GAFAM y sont [bien résumés](https://www.francetvinfo.fr/culture/livres/internet-peut-redevenir-un-bien-commun-si-nous-nous-emparons-des-outils-alternatifs-aux-gafam-entretien-avec-maxime-guedj-co-auteur-de-declic_3919083.html)
* **Déclic** de Maxime Guedj et Anne-Sophie Jacques, je ne l'ai pas encore lu mais ça ne saurais tarder :) [Dispo ici](http://www.arenes.fr/livre/declic/)

## F.A.Q.

Plusieurs lecteurs m'ont posé des questions suite à cet article, je tente d'y répondre ici.

> Par quel biais une entreprise réalise-t-elle ses bénéfices si elle développe des logiciels libres et gratuits ?

Les business-models sont variés (et parfois problématiques). Quelques exemples (mais ça méritera un autre billet plus complet) :
* Financement des développements par des entreprises
* Financement des développements par des fonds publics (État, colectivités...)
* Prestations d'installation et de maintenance des solutions libres (e.g. aller installer et configurer Libre-Office sur un parc de machines)
* Expertises sur la mise en place (quels logiciels choisirs, comment les architecturer pour répondre au besoin)
* Prestations de formations professionelles à l'utilisation ou l'extension de l'outil
* Je suppose que certains développements sont éligibles au Crédit Impôt Recherche (CIR) ou autre subventions

> Tu évoques l'implications de tous dans le processus de commercialisation. Qu'en est-il de la problématique de la propriété intellectuelle et des droits d'auteurs?

La définition d'un logiciel libre est qu'il doit être développé sous une license de propriété intellectuelle libre. Tu dois pouvoir le copier, redistribuer, modifier pour qu'il soit qualifié de libre. Pas de droits d'auteurs, donc. Il y a là aussi beaucoup à dire, il existe tout un tas de licences libres, certaines "relax" (tu as le droit de commercialiser un outil réalisé à partir d'une brique libre) ou plus "strictes" (tout logiciel construit à partir d'une brique libre doit lui aussi être libre)

> Qu'advient-il alors de la part investissement du profit? Dans ton texte tu émets les trois voix classiques de redistribution dans les salaires sans évoquer l'investissement.

L'investissement dans l'outil de production (achat d'ordinateurs, de serveurs, développement d'outils interne) se réalise comme dans toute entreprise classique (je précise bien _"hors investissement dans l'outil de production"_)